"use strict";

$(".path-menu").on("click", "a", function (event) {
  event.preventDefault();
  let id = $(this).attr("href"),
    top = $(id).offset().top;
  $("body,html").animate({ scrollTop: top }, 1500);
});

const scrollUpBtn = $("#scrollUp");

scrollUpBtn.on("click", function (event) {
  event.preventDefault();
  let id = $(this).attr("href"),
    top = $(id).offset().top;
  $("body,html").animate({ scrollTop: top }, 1500);
});

let winH = $(window).height();

$(window)
  .scroll(function () {
    if ($(this).scrollTop() >= winH) {
      scrollUpBtn.css("display", "block");
    } else {
      scrollUpBtn.css("display", "none");
    }
  })
  .on("resize", function () {
    // If the user resizes the window
    winH = $(this).height(); // you'll need the new height value
  });

const hideHeaderBtn = $("#hideHeader");

hideHeaderBtn.on("click", function () {
  $(".header__section").slideToggle();
});
